'use strict';
import React, {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    DrawerLayoutAndroid,
    ToolbarAndroid,
    Dimensions,
    ListView,
    ToastAndroid,
    Image,
    BackAndroid

} from 'react-native';


import DeviceList from './DeviceList';
var DialogAndroid = require('react-native-dialogs');
import Drawer from './Drawer';
import TabNavigator from 'react-native-tab-navigator';

import Two from './two';
import deviceCreateByBlueTooth from './deviceCreateByBlueTooth';
import deviceCreateByID from './deviceCreateByID';
import Fileindex from './fileIndex';
import GroupView from './groupView';

var DRAWER_REF = 'drawer';
var DRAWER_WIDTH_LEFT = 56;
var toolbarActions = [


    {title: '添加设备', show: 'always'},


];

var _navigator = null;
BackAndroid.addEventListener('hardwareBackPress', function () {
    if (_navigator.getCurrentRoutes().length === 1) {
        return false;
    }
    _navigator.pop();
    return true;
});

import BarcodeScannerpage from './BarcodeScannerpage';


class firstpage extends React.Component {


    Go(id:Number) {
        if (this.props.navigator && id === 2) {
            this.props.navigator.push({
                name: 'BarcodeScannerpage',
                component: BarcodeScannerpage,
                params: {
                    navigator: this.props.navigator,
                }
            })
        }
        if (this.props.navigator && id === 0) {
            this.props.navigator.push({
                name: 'deviceCreateByBlueTooth',
                component: deviceCreateByBlueTooth,
                params: {
                    navigator: this.props.navigator,
                }
            })
        }
        if (this.props.navigator && id === 1) {
            this.props.navigator.push({
                name: 'deviceCreateByID',
                component: deviceCreateByID,
                params: {
                    navigator: this.props.navigator,
                }
            })
        }
        if (this.props.navigator && id === 3) {
            this.props.navigator.push({
                name: 'deviceCreateByBlueTooth',
                component: deviceCreateByBlueTooth,
                params: {
                    navigator: this.props.navigator,
                }
            })
        }

    }

    showDialog() {
        var dialog = new DialogAndroid();
        dialog.set({
            "items": [
                "蓝牙添加",
                "ID添加",
                "扫一扫添加",
                "iBeacon"
            ],
            "title": "添加方式选择",
            itemsCallbackSingleChoice: (id, text) => this.Go(id),
        });
        dialog.show();

        //   global.List_Device.push({
        //     "id": "11494",
        //     "title": "好用的插座",
        //     "year": "备注备注备注备注备注备注备注备注备注被准备阿斯达卡付款哈是否还啊数据库",
        //     "mpaa_rating": "PG-13",
        //     "type":"插座",
        //     "image":"./icon_drawer_head.jpg",
        // });
        //   alert(global.List_Device.length);


    }

    constructor(props) {
        super(props);


        this.state = {

            navigator: this.props.navigator,
            selectedTab: 'device'
        }


    }


    // _pressButton() {
    //    // const { navigator } = this.props;
    //     //或者写成 const navigator = this.props.navigator;
    //     //为什么这里可以取得 props.navigator?请看上文:
    //     //<Component {...route.params} navigator={navigator} />
    //     //这里传递了navigator作为props
    //     if(navigator) {
    //         navigator.push({
    //             name: 'devicesDetailLight',
    //             component: devicesDetailLight,
    //         })
    //     }

    //    //  this.refs[DRAWER_REF].closeDrawer();
    // }

    _navigation() {
        return (
            <Drawer
                navigator={this.props.navigator}
                drawer={()=>this.refs[DRAWER_REF].closeDrawer()}
            />




            // <View style={{flex: 1, backgroundColor: '#fff'}}>

            //     <Text style={{margin: 10, fontSize: 15, textAlign: 'center'}}>
            //     I'm in the Drawer!
            //     </Text>

            //  </View>
        );
    }

    render() {

        _navigator = this.props.navigator;
        return (
            <DrawerLayoutAndroid
                ref={DRAWER_REF}
                drawerWidth={Dimensions.get('window').width - DRAWER_WIDTH_LEFT}
                keyboardDismissMode="on-drag"
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={this._navigation.bind(this)}>


                <TabNavigator tabBarStyle={{ height:52}}>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'device'}
                        title="设备"
                        renderIcon={() => <Image  style ={{height:30,width:30}} source={require('./image/tab_device.png')} />}
                        renderSelectedIcon={() => <Image style ={{height:30,width:30}} source={require('./image/tab_device_select.png')} />}

                        onPress={() => this.setState({ selectedTab: 'device' })}>
                        {

                            <View style={styles.container}>
                                <ToolbarAndroid
                                    navIcon={require('./image/ic_menu_white.png') }
                                    title={'设备'}
                                    actions={toolbarActions}
                                    style={styles.toolbar}
                                    onIconClicked={() => {this.refs[DRAWER_REF].openDrawer();}}
                                    onActionSelected={this.onActionSelected.bind(this)}
                                >


                                </ToolbarAndroid>

                                <View style={styles.container}>
                                    <DeviceList
                                        navigator={this.props.navigator}
                                    />
                                </View>

                            </View>
                        }
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'file'}
                        title="文件"
                        renderIcon={() => <Image  style ={{height:30,width:30}} source={require('./image/tab_file.png')} />}
                        renderSelectedIcon={() => <Image style ={{height:30,width:30}} source={require('./image/tab_file_select.png')} />}
                        //  renderBadge={() => <CustomBadgeView />}

                        onPress={() => this.setState({ selectedTab: 'file' })}>
                        {



                            <View style={styles.container}>

                                <Fileindex
                                    drawer={() => {this.refs[DRAWER_REF].openDrawer();}}
                                    navigator={this.props.navigator}/>
                            </View>
                        }
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'task'}
                        title="任务"
                        renderIcon={() => <Image  style ={{height:30,width:30}} source={require('./image/tab_task.png')} />}
                        renderSelectedIcon={() => <Image style ={{height:30,width:30}} source={require('./image/tab_task_select.png')} />}
                        //  renderBadge={() => <CustomBadgeView />}

                        onPress={() => this.setState({ selectedTab: 'task' })}>
                        {


                            <View style={styles.container}>
                                <ToolbarAndroid
                                    navIcon={require('./image/ic_menu_white.png') }
                                    title={'任务'}
                                    actions={toolbarActions}
                                    style={styles.toolbar}
                                    onIconClicked={() => {this.refs[DRAWER_REF].openDrawer();}}
                                    onActionSelected={this.onActionSelected.bind(this)}
                                >
                                </ToolbarAndroid>

                                <View style={styles.container}>
                                    <DeviceList
                                        navigator={this.props.navigator}
                                    />
                                </View>
                            </View>

                        }
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'group'}
                        title="群组"
                        renderIcon={() => <Image  style ={{height:30,width:30}} source={require('./image/tab_group.png')} />}
                        renderSelectedIcon={() => <Image style ={{height:30,width:30}} source={require('./image/tab_group_select.png')} />}
                        //  renderBadge={() => <CustomBadgeView />}

                        onPress={() => this.setState({ selectedTab: 'group' })}>
                        {


                            <View style={styles.container}>
                                <GroupView
                                    drawer={() => {this.refs[DRAWER_REF].openDrawer();}}
                                    navigator={_navigator}
                                />
                            </View>
                        }
                    </TabNavigator.Item>
                </TabNavigator>

            </DrawerLayoutAndroid>
            // <View style={styles.container}>
            //       <ToolbarAndroid
            //       navIcon={require('./ic_menu_white.png') }
            //       title = {'设备'}
            //       actions={toolbarActions}
            //       style={styles.toolbar}
            //        onIconClicked={() => {this.refs[DRAWER_REF].openDrawer();}}
            //        onActionSelected={this.onActionSelected.bind(this)}
            //       >


            //       </ToolbarAndroid>

            //        	<View style={styles.container}>
            //        	 <DeviceList
            //        	    navigator={this.props.navigator}
            //        		/>
            //        	</View>

            // </View>

        );
    }

    // renderRow(deviceInfo,sectionID, rowID){


    // 	return(

    // 		<TouchableOpacity onPress={()=>this._pressRow(deviceInfo)}>
    //          <View style ={styles.item}>
    //               <Image source={require('./icon_drawer_head.jpg')} style={styles.icon} />
    //               <View style={styles.middle}>

    //               	<Text style={{flex:1,width:150}}>{deviceInfo.title}</Text>
    //               	<Text style={{flex:1,width:150}}>{deviceInfo.year}</Text>
    //               </View>
    //               <View style={styles.right}>
    //                  <Image source={require('./more.png')} style={styles.righticon} />
    //               </View>

    //          </View>
    //     </TouchableOpacity>
    // 		);

    // }

    //   _pressRow  (deviceInfo) {


    //        const { navigator } = this.props;

    //       if(navigator) {
    //         if(deviceInfo.type==="灯")
    //           navigator.push({
    //               name: 'devicesDetailLight',
    //               component: devicesDetailLight,
    //              params:{
    //               date:deviceInfo,
    //              }
    //           })
    //         if(deviceInfo.type==="插座")
    //           navigator.push({
    //               name: 'devicesDetailSocket',
    //               component: devicesDetailSocket,
    //              params:{
    //               date:deviceInfo,
    //              }
    //           })
    //         if(deviceInfo.type==="三连屏")
    //           navigator.push({
    //               name: 'devicesDetailScreen',
    //               component: devicesDetailScreen,
    //              params:{
    //               date:deviceInfo,
    //              }
    //           })

    //       }

    // }

    onActionSelected(position) {
        if (position === 0) {
            // List_Device.push({
            //      "id": "11494",
            //      "title": "巨大的屏幕",
            //      "year": "备注备注备注备注备注备注备注备注",
            //      "mpaa_rating": "PG-13",
            //      "type":"三连屏",
            //      "image":"./icon_drawer_head.jpg",
            //  });
            this.showDialog();


        }
        // this.setState({
        //     dataSource:this.state.dataSource.cloneWithRows(List_Device),
        // });


    }


}
const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#6d9eeb',
        height: 56,
    },
    container: {
        flex: 1,
    }
    // ,
    // item:{
    // 	flexDirection:'row',
    // 	marginTop:10,
    //   marginLeft:10,
    //   marginRight:10,
    //   height:80,

    // },
    // icon:{

    // 	marginTop:20,
    // 	height:50,
    // 	width:50,
    // borderRadius:25

    // },
    // middle:{
    // 	flex:2,
    //      flexDirection:'column',
    //      marginLeft:20,
    //      marginTop:10,
    // },

    //    righticon:{
    //    	height:20,
    //    	width:20,
    //    	rotation:-90,
    //    	marginTop:30
    //    }

});
export default firstpage