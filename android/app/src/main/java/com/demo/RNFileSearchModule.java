package com.demo;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import java.util.ArrayList;
import java.util.List;

import com.demo.model.FileModel;
import com.demo.util.FileUtil;

/**
 * Created by Jin on 2016/3/8.
 */
public class RNFileSearchModule extends ReactContextBaseJavaModule {
    public RNFileSearchModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }
    public List<FileModel> fileList = new ArrayList<FileModel>();
    public FileUtil fileUtil = new FileUtil(getReactApplicationContext());

    @Override
    public String getName() {
        return "RNFileSearch";
    }

    @ReactMethod
    public void getResult(String url,final Callback callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    fileList = fileUtil.getImage();
                
                    String result0 = fileList.get(0).displayName;
        
                    String result1 = fileList.get(5).displayName;
                  
                  //  Thread.sleep(1000);//模拟网络请求
                    callback.invoke(fileList.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}