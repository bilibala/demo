'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  ListView,
  BackAndroid,
  TouchableOpacity
} from 'react-native';

var FirstView = React.createClass({

  goSecond(){
    this.props.navigator.push({name:'second'});
  },

  render(){
      return (
        <View style={{height:50,backgroundColor:'red',flexDirection:'row',justifyContent:'center'}}>
          <TouchableOpacity onPress={this.goSecond}>
          <Text style={{margin:15}}>分类</Text>
          </TouchableOpacity>
          <Text style={{flex:1,textAlign:'center',top:15}}>文件</Text>
          <TouchableOpacity >
          <Text style={{margin:15}}>编辑</Text>
          </TouchableOpacity>
        </View>

       
      );
  }

});

const styles = StyleSheet.create({
  drawerContainer: {
      width:300,
      height:150,
      backgroundColor: 'transparent'
    },
  bg: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: 300,
        height: 150,
    },
 thumbnail: {
      marginTop:5,
      left: 10,
      marginBottom: 5,
      width: 40,
      height: 40,
      borderRadius: 20,
    },
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FAEBD7'
    },
    rightContainer: {
      flex: 1,
    },
    title:{
      fontSize: 10,
      marginBottom: 8,
      textAlign: 'center',
    },
    year:{
      textAlign: 'center',
    },
    listView: {
    paddingTop: 0,
    backgroundColor: '#F5FCFF',
    },
});

module.exports=FirstView;