'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  BackAndroid,
  TouchableOpacity,
  ListView,
  Navigator,
  Image,
  TouchableHighlight,
  Dimensions,
} from 'react-native';

import Modal from 'react-native-root-modal';

class MainScreen extends Component{

  constructor(props){
    super(props);
    this.state = {
      visible: false,
      sort: {name:'分类'},
      edit: false,   
    };
  }
  showModal() {
    if(this.state.visible == false){
        this.setState({
            sort:{name:'关闭'},
            visible: true
        });
    }else{
        this.setState({
            sort:{name:'分类'},
            visible: false
        });
    }
  };

  hideModal() {
      this.setState({
        visible: false
      })
  };
  render(){
    return(

      <View style={{flex:1}}>

       <View style={{height:50,backgroundColor:'black',flexDirection:'row',justifyContent:'center'}}>
          <TouchableOpacity onPress={this.showModal.bind(this)}>
          <Text style={{margin:15,color:'white'}}>{this.state.sort.name}</Text>
          </TouchableOpacity>
          <Modal
                style={styles.modal}
                visible={this.state.visible}
            >
                <View style={{flex:1,width:Dimensions.get('window').width,height:100}}> 
                  <View style={{flexDirection:'row',height:50}}>
                   <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                  </View>
                  <View style={{flexDirection:'row',height:50}}>
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                     <Image
                     style={{flex:1,width:50,height:50}}
                     source={require('./img/recentUploadoff.png')} />
                  </View>
                </View>
        
            </Modal>


          <Text style={{flex:1,textAlign:'center',top:15,color:'white'}}>文件</Text>
          <TouchableOpacity onPress={this.goEdit.bind(this)}>
          <Text style={{margin:15,color:'white'}}>编辑</Text>
          </TouchableOpacity>
       </View>
      </View>
    );
  }
  
AppRegistry.registerComponent('RootModalExample', () => Example);