'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  BackAndroid,
  ListView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions, 
  
} from 'react-native';

var RNFileSearch = require('./RNFileSearch');
var MainScreen = require('./MainScreen');

var width = Dimensions.get('window').width;
var ds = new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2});
var Datas =  
          [{
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个幻灯片","year": "2016-02-19","selected": false,"type": "slide"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个图片","year": "2016-02-19","selected": false,"type": "pic"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一首歌","year": "2016-02-19","selected": false,"type": "music"
          }];

class ListComponent extends Component{

  constructor(props){
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(Datas),
      allSelect: false,
      titleRight:'全选',
      unuploaded: true,
      uploaded: false,
      checkRows: [],
    };
  }
 
  allSelect(){
    if(this.state.allSelect===false){
      this.state.allSelect = true;
      for(var i=0;i<Datas.length;i++){
        Datas[i].selected = true;
      }
      this.setState({allSelect:true,titleRight:'全不选'});
    }else{
      this.state.allSelect = false;
      for(var i=0;i<Datas.length;i++){
        Datas[i].selected = false;
      }
      this.setState({allSelect:false,titleRight:'全选'});
    }
    
  }
  toast(){
    console.log("test","nihao");
    RNFileSearch.getResult(
      "http://baidu.com",
      (result:Object)=>{
        if(result==null){
          console.log("结果为空");
        }else{
          console.log("结果不为空");
          console.log("callback",result);
        }
        
      }
    );
  }

 

  _pressRowForEdit(file,rowID){
      file.selected=(file.selected==true?false:true);
      this.setState({checkRows:this.state.checkRows});
  }

   _renderRowForEdit(file,sectionID,rowID) {
        var check = [];
        check = this.state.checkRows;
        file.selected == true;
        var select;
        if(file.selected == true){
          select = <View>
                    <Image
                      style={{width:10,height:10,marginRight:15}}
                      source={require('./img/check.png')} />
                   </View>;
        }else{
           select = <View>
                    <Image
                      style={{width:10,height:10,marginRight:15}}
                      source={require('./img/unchecked.png')} />
                   </View>;
        }
        return (
            <TouchableOpacity onPress={()=>this._pressRowForEdit(file,rowID)}>
              <View style={styles.container}>
                <Image
                  source={require('./img/iconhead.png')}
                  style={styles.thumbnail}
                  ></Image>
                <View style={styles.rightContainer}>
                  <Text style={styles.title}>{file.title}</Text>
                  <Text style={styles.year}>{file.year}</Text>
                </View>
                {
                  select
                }
              </View>
            </TouchableOpacity>
          );
  }



  goback(){
        const { navigator } = this.props;
         // const navigator = this.props.navigator;
        //为什么这里可以取得 props.navigator?请看上文:
        //<Component {...route.params} navigator={navigator} />
        //这里传递了navigator作为props
        if(navigator) {
            navigator.pop();
        }else{
         
        }
  }
  clickFinish(){

  }
 
  goUnupload(){
    if(this.state.unuploaded===false){
      this.state.uploaded = false;
      this.setState({unuploaded:true});
    }
  }
  goUpload(){
    if(this.state.uploaded===false){
      Datas.splice(1,2);
      this.state.unuploaded = false;
      this.setState({uploaded:true,dataSource:ds.cloneWithRows(Datas)});
    }
  }

  render(){
    return (
      <View style={{flex:1}}>
        <View style={{height:50,flexDirection:'row',backgroundColor:'#6d9eeb'}}>
        <TouchableOpacity onPress={this.goback.bind(this)}>
          <View style={{justifyContent:'center',marginLeft:15}}>
            <Image
              style={{width:30,height:30,marginTop:12}}
              source={require('./img/back.png')} />
          </View>
          </TouchableOpacity>
          <View style={{flex:1,justifyContent:'center'}}>
            <Text style={{left:120,color:'white'}}>选择文档</Text>
          </View>
          
          <View style={{justifyContent:'center',marginRight:15}}>
            <TouchableOpacity onPress={this.allSelect.bind(this)}>
             <Text style={{textAlign:'center',color:'white'}}>{this.state.titleRight}</Text>
            </TouchableOpacity>
          </View>
        </View>
        
        <View style={{flexDirection:'row',height:40}}>
        <TouchableOpacity onPress={this.goUnupload.bind(this)}>
          <Image
            style={{width:width/2,height:40}}
            source={require('./img/select/bgl.png')} >
            <View style={{marginLeft:100,marginTop:10}}>
            <Text style={[this.state.unuploaded && styles.selectedTab]}>未上传({Datas.length})</Text>
            </View>
          </Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.goUpload.bind(this)}>
          <Image
            style={{width:width/2,height:40}}
            source={require('./img/select/bgr.png')} >
            <View style={{marginLeft:25,marginTop:10}}>
            <Text style={[this.state.uploaded && styles.selectedTab]}>已上传({Datas.length})</Text>
            </View>
          </Image>
          </TouchableOpacity>
        </View>
        
        <View style={{flex:1}}>
        <ListView
          style={{flex:1}}
          dataSource={this.state.dataSource}
          renderRow={this._renderRowForEdit.bind(this)} />
        </View>
        <TouchableOpacity onPress={this.goback.bind(this)}>
        <View style={{backgroundColor:'#FFFFFF',height:50,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
          <Text style={[styles.finish]}>完成</Text>
          <Text style={{width:5}}/>
          <Image
            style={{width:10,height:10}}
            source={require('./img/check.png')} />
        </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
 thumbnail: {
      marginTop:5,
      left: 10,
      marginBottom: 5,
      width: 40,
      height: 40,
      borderRadius: 20,
    },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightContainer: {
    flex: 1,
  },
  title:{  
    marginBottom: 8,
    marginLeft:30,
  },
  year:{
    fontSize: 10, 
    marginLeft:30,
  },
  finish:{
    color:'#33AA68'
  },
  selectedTab:{
    color:'#33AA68',
  }
});

module.exports = ListComponent;