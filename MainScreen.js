'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  BackAndroid,
  TouchableOpacity,
  ListView,
  Navigator,
  Image,
  TouchableHighlight,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';

import Modal from 'react-native-root-modal';

var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';
var SortView = require('./SortView');
//var SecondView = require('./SecondView');
//import SortView from'./SortView';



var width = Dimensions.get('window').width;
var ToolbarAndroid = require('ToolbarAndroid');
var DefaultDatas = [
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个幻灯片","year": "2016-02-19","selected": false,"type": "slide"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个图片","year": "2016-02-19","selected": false,"type": "pic"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一首歌","year": "2016-02-19","selected": false,"type": "music"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个视频","year": "2016-02-19","selected": false,"type": "video"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个标签","year": "2016-02-19","selected": false,"type": "label"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一首歌","year": "2016-02-19","selected": false,"type": "music"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
          {
              "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
          },
        ];

class MainScreen extends Component{

  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(DefaultDatas),
      loaded: true,
      visible: false,  //判断中间弹窗
      opVisible: false,  //判断底部弹窗
//      sort: {name:'分类'},
      edit: false,
      checkRows: [],
      menuVisible: false  //判断顶部弹窗
    };
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if(this.state.visible===true){
        this.state.visible = false;
        this.setState({visible:false});
        return true;
      }
      if(this.state.edit===true){
        this.state.edit = false;
        this.setState({edit:false});
        return true;
      }
      if(this.state.menuVisible===true){
        this.state.menuVisible = false;
        this.setState({menuVisible:false});
        return true;
      }
      
    });
  }
  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress');
  }


  goSort() {
    if(this.state.menuVisible === false){
      this.setState({
          sort:{name:'关闭'},
          menuVisible: true,
      });
    }else{
      this.setState({
          sort:{name:'分类'},
          menuVisible: false,
      });
    }
  };

  goEdit(){
    for(var i=0;i<DefaultDatas.length;i++){
      if(DefaultDatas[i].selected == true){
        DefaultDatas[i].selected = false;
      }
    }

    if(this.state.edit == false){
        this.setState({edit: true,opVisible: true});
    }else{
        this.setState({edit: false,opVisible: false});
    }
  }

  goFile(){
    var fileDatas = [];
    for(var i = 0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].type === "file"){
          fileDatas.push(DefaultDatas[i]);
        }
    }
    this.setState({dataSource:this.state.dataSource.cloneWithRows(fileDatas),menuVisible: false,sort:{name: '分类'}});
  }
  
  goSlide(){
    var slideDatas = [];
    for(var i=0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].type === "slide"){
          slideDatas.push(DefaultDatas[i]);
        }
    }
    this.setState({dataSource:this.state.dataSource.cloneWithRows(slideDatas),menuVisible: false,sort:{name: '分类'}});
  }
  goPic(){
    var picDatas = [];
    for(var i=0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].type === "pic"){
          picDatas.push(DefaultDatas[i]);
        }
    }
    this.setState({dataSource:this.state.dataSource.cloneWithRows(picDatas),menuVisible: false,sort:{name: '分类'}});
  }

  goMusic(){
    var musicDatas = [];
    for(var i=0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].type === "music"){
          musicDatas.push(DefaultDatas[i]);
        }
    }
    this.setState({dataSource:this.state.dataSource.cloneWithRows(musicDatas),menuVisible: false,sort:{name: '分类'}});
  }

  goVideo(){
    var videoDatas = [];
    for(var i=0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].type === "video"){
          videoDatas.push(DefaultDatas[i]);
        }
    }
    this.setState({dataSource:this.state.dataSource.cloneWithRows(videoDatas),menuVisible: false,sort:{name: '分类'}});
  }
  goLabel(){
    var labelDatas = [];
    for(var i=0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].type === "label"){
          labelDatas.push(DefaultDatas[i]);
        }
    }
    this.setState({dataSource:this.state.dataSource.cloneWithRows(labelDatas),menuVisible: false,sort:{name: '分类'}});
  }
  
  goRecent(){
    this.setState({dataSource:this.state.dataSource.cloneWithRows(DefaultDatas),menuVisible: false,sort:{name: '分类'}});
  }

  goSelect(){
    this.state.visible = false;
   // alert(this.props.navigator);
    this.props.navigator.push({
      name:'sortview',
      component:SortView
  });

   // this.props.navigator.push({
   //              name: 'deviceCreateByBlueTooth',
   //              component: Two
                
   //          })

  }

  onActionSelected(position){
    if(position===0){
      if(this.state.menuVisible === false){
        this.setState({
            menuVisible: true,
        });
      }else{
        this.setState({
            menuVisible: false,
        });
      }
    }

    if(position===1){
      for(var i=0;i<DefaultDatas.length;i++){
        if(DefaultDatas[i].selected == true){
          DefaultDatas[i].selected = false;
        }
      }

      if(this.state.edit == false){
          this.setState({edit: true,opVisible: true});
      }else{
          this.setState({edit: false,opVisible: false});
      }
    }

    if(position===2){
      if(this.state.visible==false){
        this.setState({visible:true});
      }else{
        this.setState({visible:false});
      }
    }
  }
  
  render() {
    var menu;
    if(this.state.menuVisible===true){
    menu = <View style={{height:100,marginBottom:10}}> 
                  <View style={{flexDirection:'row',height:50,marginTop:5}}>
                     <TouchableOpacity onPress={this.goRecent.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/recentUploadoff.png')} />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goFile.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/fileoff.png')} />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goSlide.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/slideoff.png')} />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goPic.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/picoff.png')} />
                     </TouchableOpacity>
                  </View>
                  <View style={{flexDirection:'row',height:50,marginTop:5,marginBottom:5}}>
                     <TouchableOpacity onPress={this.goMusic.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/musicoff.png')} />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goVideo.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/videooff.png')} />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goLabel.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/labeloff.png')} />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goRecent.bind(this)}>
                       <Image
                         style={styles.modalImgStyle}
                         source={require('./img/recentUploadoff.png')} />
                     </TouchableOpacity>
                  </View>
                  <Text style={{backgroundColor:'#E9E9E9',height:1}}/>
                </View>
              }
          
    if (!this.state.loaded) {
     return this.renderLoadingView(); 
    }





    return(
      <View style={{flex:1}}>
       
          <ToolbarAndroid 
          navIcon={require('./img/ic_menu_white.png')}
          title={'文件'}
          style={{height:56,backgroundColor: '#6d9eeb'}}
          actions={[{title: '分类',  show: 'never'},
          {title: '编辑',  show: 'never'},
          {title: '上传',  show: 'never'}]}
          onIconClicked={this.onIconClicked.bind(this)}
          onActionSelected={this.onActionSelected.bind(this)}/>

          <Modal
            style={{backgroundColor:'rgba(0,0,0,0.8)',top:0,bottom:0,left:0,right:0}}
            visible = {this.state.visible}>
            <View style={{marginTop:100,marginLeft:40,marginRight:40,backgroundColor:'#EFEFEF'}}>
              <View style={{alignItems:'center',justifyContent:'center'}}>
              <View><Text style={{textAlign:'center',marginTop:20,color:'#000000'}}>选择上传类型</Text></View>
              <View>
              <Image style={{height:1,width:230,marginTop:20}} source={require('./img/divide.png')}></Image>   
              </View>
              <View style={{flexDirection:'row',marginTop:10}}>
                <TouchableOpacity onPress={this.goSelect.bind(this)}>
                <Image
                  style={{width:77,height:70}}
                  source={require('./img/fileoff.png') } />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.goSelect.bind(this)}>
                <Image
                  style={{width:76,height:70}}
                  source={require('./img/slideoff.png') } />
                  </TouchableOpacity>
                <TouchableOpacity onPress={this.goSelect.bind(this)}>
                <Image
                  style={{width:77,height:70}}
                  source={require('./img/picoff.png') } />
                </TouchableOpacity>
              </View>
              <View style={{flexDirection:'row',marginBottom:10}}>
                <TouchableOpacity onPress={this.goSelect.bind(this)}>
                <Image
                  style={{width:77,height:70}}
                  source={require('./img/musicoff.png') } />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.goSelect.bind(this)}>
                <Image
                  style={{width:76,height:70}}
                  source={require('./img/videooff.png') } />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.goSelect.bind(this)}>
                <Image
                  style={{width:77,height:70}}
                  source={require('./img/labeloff.png') } />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          </Modal>  
         
        {
          menu
        }
        { !this.state.edit  &&  
          <ListView
            dataSource={this.state.dataSource}
            renderRow={this._renderRow.bind(this)}
            automaticallyAdjustContentInsets={true} 
          ></ListView>
        }
   
        { this.state.edit && 
          <View style={{flex:1,marginTop:2}}>
            <ListView
              dataSource={this.state.dataSource}
              renderRow={this._renderRowForEdit.bind(this)}
              automaticallyAdjustContentInsets={true} 
            ></ListView>
            <View><Text style={{height:1,backgroundColor:'#E9E9E9'}}/></View>
            <View style={{backgroundColor:'#FFFFFF',height:50,flexDirection:'row'}}> 

              <View style={styles.modalImg}>
                <Image
                  style={styles.modalImgSize}
                  source={require('./img/download.png')} />
              </View>
              <View style={styles.modalImg}>
                <Image
                  style={styles.modalImgSize}
                  source={require('./img/share.png')} />
              </View> 
              <View style={styles.modalImg}>
                <Image
                  style={styles.modalImgSize}
                  source={require('./img/port.png')} />
              </View>
              <View style={styles.modalImg}>
                <Image
                  style={styles.modalImgSize}
                  source={require('./img/delete.png')} />
              </View>
              <View style={styles.modalImg}>
                <Image
                  style={styles.modalImgSize}
                  source={require('./img/label.png')} />
              </View>   
            </View>

          </View>
        }
       
      </View>
      
    );
  }

  onIconClicked(){

     this.props.drawer();
}

  renderLoadingView() {
    return (
      <ToolbarAndroid 
          logo={require('./img/ic_menu_white.png')}
          title="文件"
          style={{height:56,backgroundColor: '#6d9eeb'}}
          actions={[{title: '分类', icon:require('./img/check.png'), show: 'never'},
          {title: '编辑', icon:require('./img/check.png'), show: 'never'},
          {title: 'Settings', icon:require('./img/check.png'), show: 'never'}]}
          onActionSelected={this.onActionSelected.bind(this)}>
          </ToolbarAndroid>
     
    );
  }
  _pressRow(movie,rowID){
    //正常状态下按每一行
     alert(rowID);
  }

  _pressRowForEdit(movie,rowID){
      movie.selected=(movie.selected==true?false:true);
      this.setState({checkRows:this.state.checkRows});
  }

  _renderRow(movie: string,sectionID: number,rowID: number) {
      return ( 
        <TouchableOpacity onPress={()=>this._pressRow(movie,rowID)}>
          <View style={styles.container}>
            <Image
              source={require('./img/iconhead.png')}
              style={styles.thumbnail}
              ></Image>
            <View style={styles.rightContainer}>
              <Text style={styles.title}>{movie.title}</Text>
              <Text style={styles.year}>{movie.year}</Text>
            </View>
          </View>
        </TouchableOpacity>
      );
  }
  
  midMenu(){
    alert("nihao");
    if(this.state.visible==false){
      this.setState({visible:true});
    }else{
      this.setState({visible:false});
    }
  }
 
  _renderRowForEdit(movie,sectionID,rowID) {
        var check = [];
        check = this.state.checkRows;
        movie.selected == true;
        var select;
        if(movie.selected == true){
          select = <View>
                    <Image
                      style={{width:10,height:10,marginRight:15}}
                      source={require('./img/check.png')} />
                   </View>;
        }else{
           select = <View>
                    <Image
                      style={{width:10,height:10,marginRight:15}}
                      source={require('./img/unchecked.png')} />
                   </View>;
        }
        return (
            <TouchableOpacity onPress={()=>this._pressRowForEdit(movie,rowID)}>
              <View style={styles.container}>
                <Image
                  source={require('./img/iconhead.png')}
                  style={styles.thumbnail}
                  ></Image>
                <View style={styles.rightContainer}>
                  <Text style={styles.title}>{movie.title}</Text>
                  <Text style={styles.year}>{movie.year}</Text>
                </View>
                {
                  select
                }
              </View>
            </TouchableOpacity>
          );
  }
}

const styles = StyleSheet.create({
  drawerContainer: {
      width:300,
      height:150,
      backgroundColor: 'transparent'
    },
  bg: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: 300,
        height: 150,
    },
 thumbnail: {
      marginTop:13,
      left: 10,
      marginBottom: 5,
      width: 40,
      height: 40,
      borderRadius: 20,
    },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightContainer: {
    flex: 1,
  },
  title:{  
    marginBottom: 8,
    marginTop:3,
    marginLeft:30,
  },
  year:{
    fontSize: 10,
    marginLeft:30,
  },
  modal: {
    top: 50,
    width: Dimensions.get('window').width,
    height: 100,       
    backgroundColor: 'red',    //rgba(58, 93, 15, 0.8)   
    overflow: 'visible'
  },
  modalImg: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  modalImgSize: {
    width: 30,
    height: 30 
  },
  modalImgStyle: {
    height:50,
    width:width/4,
    alignItems:'center',
    justifyContent:'center'
  }
 
 
});

module.exports = MainScreen;
