package com.demo.util;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.demo.model.FileModel;

import java.util.ArrayList;
import java.util.List;


public class FileUtil {

    Context context;

    String[] projection = {
            MediaStore.Files.FileColumns.DATA,        //用来转化为hash值
            MediaStore.Files.FileColumns.SIZE,        //大小
            MediaStore.Files.FileColumns.DATE_ADDED,  //日期
            MediaStore.Files.FileColumns.MIME_TYPE,   //文件类型
            MediaStore.Files.FileColumns.TITLE,  //标题
            MediaStore.Files.FileColumns._ID,  //文件id
            MediaStore.Files.FileColumns.DATE_MODIFIED, //修改日期
            MediaStore.Files.FileColumns.TITLE //去后缀名标题
    };

    public FileUtil(Context context) {
        this.context = context;
    }

    public List<FileModel> getWPS() {
        List<FileModel> wpsList = null;
        String[] wpsSufs = {"%.txt","%.html","%.doc","%.xsl"};
        wpsList = getResult(wpsSufs);
        return wpsList;
    }


    public List<FileModel> getImage() {
        List<FileModel> imageList = new ArrayList<>();
        String[] imageSufs ={"%.png","%.jpg","%.jpeg","%.gif"};
        imageList = getResult(imageSufs);
        return imageList;
    }

    public List<FileModel> getVideo() {
        List<FileModel> videoList = null;
        String[] videoSufs = {"%.avi","%.rmvb","%.wmv","%.mkv","%.mp4"};  // *.rm *.asf *.divx *.mpg *.mpeg *.mpe *.wmv *.vob
        videoList = getResult(videoSufs);
        return videoList;
    }

    public List<FileModel> getAudio() {
        List<FileModel> audioList = null;
        String[] audioSufs = {"%.mp3","%.mid"};
        audioList = getResult(audioSufs);
        return audioList;
    }

    public List<FileModel> getPPT() {
        List<FileModel> pptList = null;
        String[] pptSufs = {"%.ppt", "%.pptx"};
        pptList = getResult(pptSufs);
        return pptList;
    }

    /**
     *
     * @extensionTypes 后缀名数组的变量名
     * @return
     */
    public List<FileModel> getResult(String[] extensionTypes) {
        List<FileModel> tempList = new ArrayList<FileModel>();
        String selectionMimeType = MediaStore.Files.FileColumns.DATA + " like?";
        for (int i = 0; i < extensionTypes.length-1; i++) {
            selectionMimeType += " OR "+MediaStore.Files.FileColumns.DATA + " like?";
        }

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Files.getContentUri("external"),
                projection,
                selectionMimeType,
                extensionTypes,
                null);
        try {
            if (cursor != null) {
                int dataIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
                int sizeIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.SIZE);
                int dataAddIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATE_ADDED);
                int mimetypeIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE);
                int displayNameIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.TITLE);
                if (cursor.moveToFirst()) {
                    do {
                        FileModel fileModel = new FileModel();
                        fileModel.hash = cursor.getString(dataIndex);
                        fileModel.size = cursor.getString(sizeIndex);
                        fileModel.dateAdd = cursor.getString(dataAddIndex);
                        fileModel.mimeType = cursor.getString(mimetypeIndex);
                        fileModel.displayName = dataToTitle(cursor.getString(dataIndex));
                        tempList.add(fileModel);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("cursor 为空！");
        }
        return tempList;
    }

    /**
     *
     * @param data 将data属性转化为标题
     * @return
     */
    public String dataToTitle(String data){
       int lastTabIndex = data.lastIndexOf("/")+1;
       String title = data.substring(lastTabIndex);
        return title;
    }

}
