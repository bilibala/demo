"use strict";

import React, {
    View,
    Component,
    StyleSheet,
    Text,
    ToolbarAndroid,
} from 'react-native'

class GroupInfo extends Component {


    constructor() {
        super()
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolbarAndroid
                    navIcon={require('./image/ic_menu_white.png') }
                    title="群组信息"
                    style={styles.toolbar}/>
                
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    toolbar: {
        backgroundColor: '#6d9eeb',
        height: 56,
    },
});

export default GroupInfo;