'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  BackAndroid,
  ListView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
var ListComponent = require('./ListComponent');
// var Datas =  
//           [{
//               "thumbnail": "./img/iconhead.png","title": "这是一个文件","year": "2016-02-19","selected": false,"type": "file"
//           },
//           {
//               "thumbnail": "./img/iconhead.png","title": "这是一个幻灯片","year": "2016-02-19","selected": false,"type": "slide"
//           },
//           {
//               "thumbnail": "./img/iconhead.png","title": "这是一个图片","year": "2016-02-19","selected": false,"type": "pic"
//           },
//           {
//               "thumbnail": "./img/iconhead.png","title": "这是一首歌","year": "2016-02-19","selected": false,"type": "music"
//           }];

class SortView extends Component{

  constructor(props){
    super(props);
    this.state = (
      {
        navigator:this.props.navigator,
      }
    );
  }
  
  onpressFile(){
    // const { navigator } = this.props;
    //     //或者写成 const navigator = this.props.navigator;
    //     //为什么这里可以取得 props.navigator?请看上文:
    //     //<Component {...route.params} navigator={navigator} />
    //     //这里传递了navigator作为props
    //     if(navigator) {
    //         navigator.push({
    //             name: 'second',
    //         });
    //     }
 
   // this.state.navigator.push({name:'main'});
  }
  render(){
    return (
      <ListComponent navigator={this.state.navigator}/>
    );
  }
}

const styles = StyleSheet.create({
 thumbnail: {
      marginTop:5,
      left: 10,
      marginBottom: 5,
      width: 40,
      height: 40,
      borderRadius: 20,
    },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightContainer: {
    flex: 1,
  },
  title:{  
    marginBottom: 8,
    marginLeft:30,
  },
  year:{
    fontSize: 10, 
    marginLeft:30,
  },
});

module.exports = SortView;