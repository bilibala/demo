'use strict';
import React,{
    View,
    Text,
    StyleSheet,
    ToolbarAndroid,
    Switch
} from 'react-native';

 




 
 class deviceCreateByBlueTooth extends React.Component {
     constructor(props) {
         super(props);
         this.state = {
     isopen:false,
       navigator :this.props.navigator
    
    };
     }

     render() {
         return ( 
        <View style={styles.container}>
           <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha') }
                       title = {'蓝牙添加'}
                      
                       style={styles.toolbar}
                        onIconClicked={this.state.navigator.pop}
                        
                       >
                           
                            
                       </ToolbarAndroid>
                       
                          <View style={styles.container}>
                             <View style={styles.buttonarea}>
                                <View style={{marginLeft:20}} >
                                  <Text style={styles.text}>蓝牙</Text>
                                  <Text>已扫描到的设备</Text>
                                </View>
                                 <Switch  style={{flex:1,marginRight:20}}
                                  disabled={false}
                                  onValueChange={this.onValueChange.bind(this)}
                                  value={this.state.isopen}

                                  ></Switch> 
                             </View>
                             <View style={styles.space}>
                               

                             </View>
                          </View>
        </View>
        )
     }

     onValueChange(value){

   

     this.setState({
       isopen:value,
     });

  }
 }
 
 const styles = StyleSheet.create({
   toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  container:{
    flex:1,
    backgroundColor:'#ffffff',
    flexDirection:'column'
  },
  buttonarea:{
    height:80,
    flexDirection:'row'
  },
  text:{
    marginTop:24,fontSize:19,color :'#70d238'
  },
  space:{height:2,
    backgroundColor:'#a9ec82',
    marginLeft:15,
    marginRight:15
    }
 });
 
 export default deviceCreateByBlueTooth;
 