package com.demo.model;

/**
 * Created by Jin on 2016/3/9.
 */
public class FileModel {
    public String mimeType;
    public String hash;
    public String displayName;
    public String dateAdd;
    public String size;
}
