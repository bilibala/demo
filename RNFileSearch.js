'use strict';
var { NativeModules } = require('react-native');

var RCTRNFileSearch= NativeModules.RNFileSearch;

var RNFileSearch = {

  getResult:function(
    url: string,
    callback:Function,
  ): void {
    RCTRNFileSearch.getResult(url,callback);
  },
};

module.exports = RNFileSearch;