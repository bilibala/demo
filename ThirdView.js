'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  BackAndroid,
  TouchableOpacity
} from 'react-native';

var ThirdView = React.createClass({
  goDefault(){
      this.props.navigator.push({name:"default"});
  },
  render(){
      return (
          <TouchableOpacity style={styles.container} onPress={this.goDefault}>
              <Text style={styles.welcome}>third view</Text>
          </TouchableOpacity>
      );
    }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

module.exports = ThirdView;

