/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  BackAndroid,
  TouchableOpacity
} from 'react-native';
var FirstView = require('./FirstView');
var SecondView = require('./SecondView');
var ThirdView = require('./ThirdView');
var MainScreen = require('./MainScreen');
var SortView = require('./SortView');

class fileIndex extends React.Component {

  constructor(props){
    super(props);
  }

  // configureScene(route){
  //   return Navigator.SceneConfigs.FadeAndroid;
  // }

  // renderScene(router,navigator){
  //   var Component = null;

  // //  this._navigator = navigator;
  //   switch(router.name){
  //     case "main":
  //       Component = MainScreen;
  //       break;
  //     case "second":
  //       Component = SecondView;
  //       break;
  //     case "third":
  //       Component = ThirdView;
  //       break;
  //     case "sortview":
  //       Component = SortView;
  //       break;
  //     default:
  //       Component = null;
  //       break;
  //   }

  //   return <Component navigator={navigator}/>
  // }

  componentWillUnmount() {
    //BackAndroid.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
      // var navigator = this._navigator;
      // BackAndroid.addEventListener('hardwareBackPress', function() {
      //     if (navigator && navigator.getCurrentRoutes().length > 1) {
      //       navigator.pop();
      //       return true;
      //     }
      //     return false;
      // });
  }

  render() {

    const { navigator } = this.props;

    return (

      <MainScreen
       drawer={this.props.drawer}
      navigator={navigator}/>
      // <Navigator
        
      //   initialRoute={{name:'main'}}
      //   configureScene={this.configureScene}
      //   renderScene={this.renderScene} />


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

module.exports =fileIndex
