/**
 * Created by Jie on 2016-04-05.
 */
"use strict";
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    ListView,
    View,
    ToastAndroid,
    Text,
    ToolbarAndroid,
}
    from 'react-native'


var toolbarActions = [{
    title: '多选',
    show: 'never',
}, {
    title: '添加好友',
    show: 'never'
}, {
    title: '添加设备',
    show: 'never'
}, {
    title: '添加任务',
    show: 'never'
}, {
    title: '上传文件',
    show: 'never'
}, {
    title: '扫一扫',
    show: 'never'
},];


var listData = [
    {
        icon: './head02.png',
        title: '群组名称（人数1）',
        time: '11:11',
        lastMessage: '这里是最新消息',
        notify: true,
        id: 0,
    }, {
        icon: './head02.png',
        title: '群组名称（人数2）',
        time: '11:11',
        lastMessage: '这里是最新消息',
        notify: true,
        id: 1,
    }, {
        icon: './head02.png',
        title: '群组名称（人数3）',
        time: '11:11',
        lastMessage: '这里是最新消息',
        notify: true,
        id: 2,
    }, {
        icon: './head02.png',
        title: '群组名称（人数4）',
        time: '11:11',
        lastMessage: '这里是最新消息',
        notify: true,
        id: 3,
    }, {
        icon: './head02.png',
        title: '群组名称（人数5）',
        time: '11:11',
        lastMessage: '这里是最新消息',
        notify: true,
        id: 4,
    }
];

var GroupItem = require('./GroupItem');
import GroupInfo from './GroupInfo';
class GroupView extends Component {

    constructor(props) {
        super(props)
        var ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.state = {
            dataSource: ds.cloneWithRows(listData),
        };
    }

    render() {
        return (

            <View style={styles.container}>
                <ToolbarAndroid
                    navIcon={require('./image/ic_menu_white.png') }
                    title="群组"
                    style={styles.toolbar}
                    actions={toolbarActions}
                    onIconClicked={() => {this.props.drawer();}}
                    onActionSelected={this.onActionSelected}/>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => 
                            <GroupItem
                             key={rowData.id}
                             story={rowData}
                             clickItem={this._press.bind(this)}/>
                    }/>
            </View>
        );
    }

    _press(id) {

        if (this.props.navigator) {
            this.props.navigator.push({
                name: 'GroupInfo',
                component: GroupInfo,
            });
        }
    }

    onActionSelected(position) {
        ToastAndroid.show(String(position), ToastAndroid.SHORT);
    }

}
var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FAFAFA',
    },
    toolbar: {
        backgroundColor: '#6d9eeb',
        height: 56,
    },
    listItem: {
        height: 56,
    },
    item_text: {
        color: '#e00000',
        textAlign: 'center',
    }
});

export default GroupView